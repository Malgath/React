

import './App.css'
function App() {
return (
  <>
  </>
)
}

export function RoboHash({nome}){
  const imgSrc = `https://robohash.org/${nome}`;
  return <img src={imgSrc} alt = "RoboHash image"/>
}
export default App
